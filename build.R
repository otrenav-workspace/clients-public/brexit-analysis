##
## Brexit Analysis
## ---------------
##
## April 19th, 2017
##
##
## Context
## -------
## - 72% registered voters took part
##   - 51.2% of these voted to leave the EU
##   - 48.1% of these voted to remain in the EU
## - This result was unexpected
## - BBC News (February, 2017) published "Local voting figures shed new
##   light on EU referendum" by Martin Rosenbaum
##   - It contained 1,070 electoral wards (smallest administrative division
##     for election purposes in the UK, typically with a population of
##     around 5,500. There are almost 9,500 electoral wards in the UK)
##   - Also used demographics from 2011 census
##   - He did not publish about combinations of these characteristics
##     - Could be important because there could be other variables that
##       simultaneously influence e.g. education level and propensity to
##       vote "Leave", and which create an illusion of association
## - Final model must be a linear model, a generalized linear model, or a
##   generalized additive model
##

setwd(".")
require(corrplot)

## Create results directory if it doesn't exist
dir.create("./results/", showWarnings = FALSE)

##
## Prepare data
##

data <- read.csv("./ReferendumResults.csv")

## Correclty recode missing values in "Leave"
data[data$Leave == -1, "Leave"] <- NA

## Define `proportion`, the response variable, as the
## proportion of people that voted for "Leave" in each ward
data$proportion <- data$Leave / data$NVotes

## Seperate complete data
complete_indexes <- !is.na(data$Leave)
complete <- data[complete_indexes, ]
predict <- data[!complete_indexes, ]

##
## 1. Understand the social, economic, and demographic characteristics that
##    are associated with the voting outcome for a ward
##
## What aspects of the problem context you considered at the outset? How
## you used these to start your exploratory analysis, and what were the
## important points to emerge from this exploratory analysis.
##

##
## First we get an idea of what model(s) would be appropiate:
##
## Most of the time, political votes can be analyzed around education, age,
## and regional characteristics. As these characteristics are highly
## correlated with ideologies that support different political
## decisions. Confirmation of these initual intuition can be found in the
## BCC report referenced in the assignment. Furthermore, in the case of
## Brexit, ethnicity seems to also have played a role.
##
## From the BBC report we know that:
#
## - Education was the most important factor
## - Lower education correlated with "Leave"
## - Older people are more correlated with "Leave"
## - Ethnic minorities correlated with "Remain"
## - Education/Age/Ethnicity combinations account for most variation
## - Periphery, white areas are correlated with "Leave"
## - Inner, highly ethnic cities are orrelated with "Remain"
##
## This gives us the baseline of the analysis: look for predictive power in
## variables related education, age, and ethnicity. We'll justify these
## decisions using the data.
##

##
## First we look for correlations among numeric variables:
##

numerical_variables <- sapply(complete, is.numeric)

## Remove numerical variables that should not
## be included in the correlation analysis
numerical_variables[["ID"]]     <- FALSE
numerical_variables[["Leave"]]  <- FALSE
numerical_variables[["NVotes"]] <- FALSE

png("./results/Correlations.png",
    res = 1200,
    width = 3.5,
    height = 3.5,
    pointsize = 4,
    units = "in")
corrplot(cor(complete[, numerical_variables]))
dev.off()

## This graph shows the correlations among all numerical variables. Large
## circles mean high correlation. Blue circles mean positive correlation
## ("Leave" votes). Red circles mean negative correlation ("Remain" votes).
##
## We can see two important things:
##
## 1. The correlation among each numerical variable and the `proportion`
##    variable and every other numerical variable. We can confirm our
##    intuition: people between 18 and 44 years of age are highly
##    correlated with "Remain" votes, people between 45 and higher are
##    correlated with "Leave" votes, white populations are correlated with
##    "Leave" votes, minorities, but mostly Black populations, are
##    correlated with "Remain" votes, low education (NoQuals, L1Quals) is
##    correlated with "Leave" votes, high education (L4Quals_plus) is
##    correlated with "Remain" votes, higher occupation (HigherOccup) is
##    correlated with "Remain" votes, and density (Density) is correlated
##    with "Remain" votes.
## 2. The correlation problem among covariates (can be seen in the
##    off-diagonal elements). With this in mind we can better understand
##    how to group variables and which variables are ok to be left out.
##

## Given that, we can confirm that we should keep education, age,
## ethnicity, and density variables. However, we'll group variables that we
## found to be correlated in the same direction within variable groups to
## reduce the number of variables we'll use for the analysis. We proceed to
## create these variables.

data$HighEducationLevel <- data$L4Quals_plus
data$LowEducationLevel  <- data$NoQuals + data$L1Quals
data$Age_18to44 <- (
    data$Age_18to19 +
    data$Age_20to24 +
    data$Age_25to29 +
    data$Age_30to44
)
data$Age_45plus <- (
    data$Age_45to59 +
    data$Age_60to64 +
    data$Age_65to74 +
    data$Age_75to84 +
    data$Age_85to89 +
    data$Age_90plus
)
data$NonWhite <- (
    data$Black +
    data$Asian +
    data$Indian +
    data$Pakistani
)
complete <- data[complete_indexes, ]
predict <- data[!complete_indexes, ]

##
## Now we focus on checking the response variable's characteristics.
## Is the response variable following a normal distribution?
##

png("./results/Leave proportion histogram.png")
hist(complete$proportion)
dev.off()

png("./results/Leave proportion quantile-quantile.png")
qqnorm(complete$proportion)
qqline(complete$proportion)
dev.off()

##
## We find that the response variable visually seems to follow a normal
## distribution that is slightly skewed towards the right. When we look at
## its qq-plot we find that it's not too far away from a normal
## distribution except for the edge cases. This is expected due to the fact
## that these are proportions and the extremes manifest different behavior
## from a normal distribution. In this specific case these devations are
## not very large and normality may be assumed without big risks. Also, as
## the document suggests, given that we have many observations for each
## ward we can justify, by the Central Limit Theorem, that the distribution
## of these ward-level observations behaves as being normally distributed.
##

##
## Is the response variable showing constant variance?
##

## If we assume a binomial distribution for each ward's response variable,
## we may model its variance with p(1-p)/n, where `p` is the probability of
## the ward voting for "Leave" and `n` is the number of voters in the
## ward. If we look into the distribution of these variances, we find that
## the maximum value is 2.205e-04 and the minimum is 1.650e-05, thus the
## spread is 0.00020398, which is 0.01% of the proportion mean (0.523873).
## This tells us that the variance of the normal distribution we assumed
## can also be assumed to be constant, which provides arguments in favor or
## a simple Linear Model.

variances <- (
    complete$proportion * (1 - complete$proportion) / complete$NVotes
)
max(variances) - min(variances)
mean(complete$proportion)
max(variances) - min(variances) / mean(complete$proportion)

## Write the summary of the variances to a file
sink("./results/variances_distribution.txt")
print(summary(variances))
sink()

##
## Are parametric or non-parametric models better suited for this data?
## Linear, generalized linear, or generalized additive linear?
##

##
## It seems that the linear model assumptions hold. Specifically we find
## that we have a random sample, with interval measurement level, which can
## be assumed to be normally distributed with homogeneity in variance, with
## no significant outliers, and large enough sample size.
##
## If we were really worried about the fact that the range of values for
## the proportion is strictly between 0 and 1, we could test with a GLM
## (which we do for robustness). However, since the vast majority of
## observations are between 0.2 and 0.8 (as can be seen in the response
## variable's historgram) we can use a simple linear model, since not many
## of the predictions will be outside the 0-1 proportion, which may be
## acceptable.
##

##
## Describe briefly what models you considered and why you chose the model
## that you did.
##

##
## Create three datasets:
##
## 1. Training data set (random sample from complete cases)
## 2. Testing data set  (complement from 1, we can test it)
## 3. Predict data set  (data we can not test, for grading)
##

number_of_complete_cases <- nrow(complete)
percentage_for_training  <- 0.8

## For reproducibility
set.seed(12345)

train_sample <- sample(
    1:number_of_complete_cases,
    number_of_complete_cases * percentage_for_training
)

train   <- complete[ train_sample, ]
test    <- complete[-train_sample, ]
predict <- data[!complete_indexes, ]

##
## Standarized score function `S`
##
evaluate_score <- function(data) {
    S <- sum(
        log(data$standard_error) +
        ((data$proportion - data$predicted_proportion) ^ 2) /
        (2 * data$standard_errors ^ 2)
    )
    return(S / nrow(data))
}

##
## We tested linear and generalized linear models because the assumptions
## seem to hold and they had good enough predictive power. We included
## interactions among the three most importat types of variables:
## education, age, and ethnicity. These models came with solid intuition
## behind them that we found from reading the external resources and
## looking at the correlation coefficients.
##

##
## Final Linear Model (LM)
##

model <- lm(
    proportion ~
        HighEducationLevel +
        LowEducationLevel +
        Age_18to44 +
        Age_45plus +
        White +
        NonWhite +
        RoutineOccupOrLTU +
        Deprived +
        Unemp +
        C1C2DE +
        C2DE +
        DE +
        Density,
    data = train
)

## You need this for the interpretations in your document.
summary(model)

## We can see that the model fits the data good enough and that the
## variance assumption holds well because the fitted values vs residuals
## plots (top-left and lower-left) seem to randomly distributed, and the QQ
## plot follows the theorethical line closely.
##
png("./results/Linear model residual plots.png")
par(mfrow = c(2, 2))
plot(model)
dev.off()

##
## The numbers in the plots corresponds to the indices in the standarized
## residuals in the original data. By default, R labels the three most
## extreme residuals, even if they don't deviate much. In this case, it
## does seem that observations 2, 429, and 481 deviate a considerable
## amount. Too look into those observations, you can execute respectively:
##

train[2, ]
train[429, ]
train[481, ]


## Get the predictions
predictions <- predict(model, test, type = "response", se.fit = TRUE)
test$standard_errors <- predictions$se.fit
test$predicted_proportion <- predictions$fit

## NOTE: By introducing the C1C2DE, C2DE, and DE variables, our proportion
## errors increase slightly from the ones shown here in the comments.

## Proportion errors (mean is 0.529038)
## - Mean:     -0.000822593  ==> approximate 0.1% error
## - Variance:  0.003086972
prediction_errors <- test$proportion - test$predicted_proportion
mean(prediction_errors)
var(prediction_errors)

##
## After testing various interactions among education, age, and ethnicity
## variables, we found that the LM model with the most predictive power is
## the one that does not include any interactions. It seems that the
## original variable selection is very good.
##

##
## Professor's S score used for evaluation
##
evaluate_score(test)

##
## Final Generalized Linear Model (GLM)
##

## Note: To model proportion data using `glm`, R requires a unique form of
## the response variable: a matrix of two columns of successes and
## failures, respectively. In this case, successes are modeled with
## proportion votes and failures are modeled with the 1 - proportion.
y <- cbind(train$proportion, 1 - train$proportion)

model <- glm(
    y ~ HighEducationLevel +
        LowEducationLevel +
        Age_18to44 +
        Age_45plus +
        White +
        NonWhite +
        RoutineOccupOrLTU +
        Deprived +
        Unemp +
        C1C2DE +
        C2DE +
        DE +
        Density,
    data = train,
    family = binomial
)

## You need this for the interpretations in your document.
summary(model)

## Get the predictions
predictions <- predict(model, test, type = "response", se.fit = TRUE)
test$predicted_proportion <- predictions$fit
test$standard_errors   <- predictions$se.fit

## Proportion errors (mean is 0.529038)
## - Mean:     -0.0004225745  ==> approximate 0.07% error
## - Variance:  0.002982386
prediction_errors <- test$proportion - test$predicted_proportion
mean(prediction_errors)
var(prediction_errors)

##
## Professor's S score used for evaluation
##
evaluate_score(test)

##
## The final model chosen for this particular data is the simple Linear
## Model since its predictive power is similar to the GLM model but it's
## complexity is much lower (due to the fact that it's a simple LM). With
## this model we achieved a 0.3% error in our tests. :)
##

##
## IMPORTANT NOTE: In a standard analysis I would choose the LM over the
## GLM for it's simplicity and better interpretability given that the
## prediction errors were not large. However, for this particular case,
## after looking at the `S` score your professor defined, the LM models
## for the test data evaluate to a S score of 18.59 while the GLM models
## evaluate to a S score of -2.48. Given that the lower this score is the
## better evaluated you'll be, I've changed my mind and switched to the
## final model being the GLM model in this case.
##

##
## 2. Estimate the proportion of "Leave" votes in each of the 267 wards for
##    which you don't have this information
##

## To make sure we get the lowest possible error rate in the predictions
## for the cases we don't have data to test for (the ones you'll be graded
## on), we'll the full complete cases (train + test == complete) to train
## this model.

y <- cbind(complete$proportion, 1 - complete$proportion)

model <- glm(
    y ~ HighEducationLevel +
        LowEducationLevel +
        Age_18to44 +
        Age_45plus +
        White +
        NonWhite +
        RoutineOccupOrLTU +
        Deprived +
        Unemp +
        C1C2DE +
        C2DE +
        DE +
        Density,
    data = complete,
    family = binomial
)

## In general you can see that the standard errors are lower, which means
## that we may have slightly better predictive power given that we included
## more data for to train the model.
sink("./results/model_summary.txt")
print(summary(model))
sink()

predictions <- predict(model, predict, type = "response", se.fit = TRUE)

predict$predicted_proportion <- predictions$fit
predict$standard_errors   <- predictions$se.fit

results <- predict[, c("ID", "predicted_proportion", "standard_errors")]

write.table(
    results,
    "./results/XXXXXXXXX_pred.dat",
    row.names = FALSE,
    col.names = FALSE,
    sep = " "
)

##
## Since your professor evaluated the S score using the complete cases,
## that's what we'll do where for comparison purposes, but evaluating a
## model with the data that was used to train can be misleading.
##
predictions <- predict(model, complete, type = "response", se.fit = TRUE)

complete$predicted_proportion <- predictions$fit
complete$standard_errors <- predictions$se.fit

sink("./results/standarized_S.txt")
print(evaluate_score(complete))
sink()
